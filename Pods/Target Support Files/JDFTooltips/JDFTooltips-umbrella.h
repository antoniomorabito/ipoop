#import <UIKit/UIKit.h>

#import "JDFSequentialTooltipManager.h"
#import "JDFTooltipManager.h"
#import "JDFTooltips.h"
#import "JDFTooltipView.h"
#import "UILabel+JDFTooltips.h"
#import "UIView+JDFTooltips.h"

FOUNDATION_EXPORT double JDFTooltipsVersionNumber;
FOUNDATION_EXPORT const unsigned char JDFTooltipsVersionString[];

