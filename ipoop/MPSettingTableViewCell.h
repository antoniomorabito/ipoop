//
//  MPSettingTableViewCell.h
//  ipoop
//
//  Created by Antonio M on 12/02/16.
//  Copyright © 2016 co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPSettingTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelMenu;
@property (strong, nonatomic) IBOutlet UIImageView *icon;

@end
