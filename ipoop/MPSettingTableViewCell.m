//
//  MPSettingTableViewCell.m
//  ipoop
//
//  Created by Antonio M on 12/02/16.
//  Copyright © 2016 co. All rights reserved.
//

#import "MPSettingTableViewCell.h"

@implementation MPSettingTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
