//
//  MPSettingsTableViewController.h
//  ipoop
//
//  Created by Antonio M on 12/02/16.
//  Copyright © 2016 co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPSettingsTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
