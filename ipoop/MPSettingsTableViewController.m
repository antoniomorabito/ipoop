//
//  MPSettingsTableViewController.m
//  ipoop
//
//  Created by Antonio M on 12/02/16.
//  Copyright © 2016 co. All rights reserved.
//

#import "MPSettingsTableViewController.h"
#import "MPSettingTableViewCell.h"
@interface MPSettingsTableViewController ()
{
    NSArray *arraySetting;
}

@end

@implementation MPSettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arraySetting =@[@"Profile",@"Notifications",@"Friends",@"Login/Signup"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return arraySetting.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MPSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MPSettings"];
    NSString *menu = [arraySetting objectAtIndex:indexPath.row];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"MPSettingsTableCell" bundle:nil] forCellReuseIdentifier:@"MPSettings"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"MPSettings" forIndexPath:indexPath];
    }
        cell.labelMenu.text = menu;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
    }
    else if (indexPath.row==1)
    {
        
    }
    else if (indexPath.row == 2)
    {
        
    }
    else if (indexPath.row ==3)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LOGIN"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
