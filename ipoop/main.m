//
//  main.m
//  ipoop
//
//  Created by Antonio M on 28/12/15.
//  Copyright © 2015 co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
